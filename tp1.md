``` bash 
PS C:\Windows\system32> ping 10.1.1.2

Envoi d’une requête 'Ping'  10.1.1.2 avec 32 octets de données :
Réponse de 10.1.1.2 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.1.1.2:
    Paquets : envoyés = 1, reçus = 1, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
Ctrl+C
PS C:\Windows\system32> ssh
usage: ssh [-46AaCfGgKkMNnqsTtVvXxYy] [-B bind_interface]
           [-b bind_address] [-c cipher_spec] [-D [bind_address:]port]
           [-E log_file] [-e escape_char] [-F configfile] [-I pkcs11]
           [-i identity_file] [-J [user@]host[:port]] [-L address]
           [-l login_name] [-m mac_spec] [-O ctl_cmd] [-o option] [-p port]
           [-Q query_option] [-R address] [-S ctl_path] [-W host:port]
           [-w local_tun[:remote_tun]] destination [command]
PS C:\Windows\system32> ssh 10.1.1.2
ssh: connect to host 10.1.1.2 port 22: Connection timed out
PS C:\Windows\system32> ssh 10.1.1.2
ssh: connect to host 10.1.1.2 port 22: Connection timed out
PS C:\Windows\system32> ssh mathis@10.1.1.2
ssh: connect to host 10.1.1.2 port 22: Connection timed out
PS C:\Windows\system32> ssh mathis@10.1.1.2
The authenticity of host '10.1.1.2 (10.1.1.2)' can't be established.
ECDSA key fingerprint is SHA256:2JHMnWrafsyx7sz4Y8Cv26PGPoVZqUBlVdjhS2qikFE.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.1.1.2' (ECDSA) to the list of known hosts.
mathis@10.1.1.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec  6 11:34:48 2021
[mathis@localhost ~]$ sudo hostname mathis.tp1.cesi
[sudo] password for mathis:
[mathis@localhost ~]$
[mathis@localhost ~]$
[mathis@localhost ~]$ hostname
mathis.tp1.cesi
[mathis@localhost ~]$ vi /etc/hostname
[mathis@localhost ~]$
[mathis@localhost ~]$
[mathis@localhost ~]$ echo "mathis.tp1.cesi" | sudo tee /etc/hostname
mathis.tp1.cesi
[mathis@localhost ~]$ hostname
mathis.tp1.cesi
[mathis@localhost ~]$ exit
logout
Connection to 10.1.1.2 closed.
PS C:\Windows\system32> ssh mathis@10.1.1.2
mathis@10.1.1.2's password:
Permission denied, please try again.
mathis@10.1.1.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last failed login: Mon Dec  6 11:49:06 CET 2021 from 10.1.1.1 on ssh:notty
There was 1 failed login attempt since the last successful login.
Last login: Mon Dec  6 11:47:07 2021 from 10.1.1.1
[mathis@mathis ~]$ hostname
mathis.tp1.cesi
[mathis@mathis ~]$ visudo /etc/sudo
sudo.conf       sudoers         sudoers.d/      sudo-ldap.conf
[mathis@mathis ~]$ visudo /etc/sudo
sudo.conf       sudoers         sudoers.d/      sudo-ldap.conf
[mathis@mathis ~]$ visudo /etc/sudoers
visudo: /etc/sudoers: Permission denied
[mathis@mathis ~]$ sudo visudo /etc/sudoers
[sudo] password for mathis:
visudo: /etc/sudoers.tmp unchanged
[mathis@mathis ~]$ groupd
-bash: groupd: command not found
[mathis@mathis ~]$ guid
-bash: guid: command not found
[mathis@mathis ~]$ man useradd
[mathis@mathis ~]$ useradd user1 --home /home/user1 --groups wheel
useradd: Permission denied.
useradd: cannot lock /etc/group; try again later.
[mathis@mathis ~]$ sudo useradd user1 --home /home/user1 --groups wheel
[mathis@mathis ~]$ su -l user1
Password:
su: Authentication failure
[mathis@mathis ~]$ passwd user1
passwd: Only root can specify a user name.
[mathis@mathis ~]$ sudo passwd user1
Changing password for user user1.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
[mathis@mathis ~]$ su -l user1
Password:
su: Authentication failure
[mathis@mathis ~]$ su -l user1
Password:
Last failed login: Mon Dec  6 11:53:34 CET 2021 on pts/0
There were 2 failed login attempts since the last successful login.
[user1@mathis ~]$
[user1@mathis ~]$
[user1@mathis ~]$
[user1@mathis ~]$ pwd
/home/user1
[user1@mathis ~]$ sudo
usage: sudo -h | -K | -k | -V
usage: sudo -v [-AknS] [-g group] [-h host] [-p prompt] [-u user]
usage: sudo -l [-AknS] [-g group] [-h host] [-p prompt] [-U user] [-u user] [command]
usage: sudo [-AbEHknPS] [-r role] [-t type] [-C num] [-g group] [-h host] [-p prompt] [-T timeout] [-u user] [VAR=value] [-i|-s]
            [<command>]
usage: sudo -e [-AknS] [-r role] [-t type] [-C num] [-g group] [-h host] [-p prompt] [-T timeout] [-u user] file ...
[user1@mathis ~]$ exit
logout
[mathis@mathis ~]$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
[mathis@mathis ~]$ setenforce 0
setenforce:  setenforce() failed
[mathis@mathis ~]$ sudo setenforce
usage:  setenforce [ Enforcing | Permissive | 1 | 0 ]
[mathis@mathis ~]$ sudo setenforce 0
[mathis@mathis ~]$ vi /etc/selinux/config
[mathis@mathis ~]$ sudo vi /etc/selinux/config
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
[mathis@mathis ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-06 11:34:31 CET; 26min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 856 (sshd)
    Tasks: 1 (limit: 4943)
   Memory: 4.2M
   CGroup: /system.slice/sshd.service
           └─856 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@op>

Dec 06 11:34:31 localhost.localdomain sshd[856]: Server listening on :: port 22.
Dec 06 11:34:31 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
Dec 06 11:47:07 localhost.localdomain sshd[1813]: Accepted password for mathis from 10.1.1.1 port 55608 ssh2
Dec 06 11:47:07 localhost.localdomain sshd[1813]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
Dec 06 11:49:04 mathis.tp1.cesi sshd[1871]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=1>
Dec 06 11:49:06 mathis.tp1.cesi sshd[1871]: Failed password for mathis from 10.1.1.1 port 55658 ssh2
Dec 06 11:49:09 mathis.tp1.cesi sshd[1871]: Accepted password for mathis from 10.1.1.1 port 55658 ssh2
Dec 06 11:49:09 mathis.tp1.cesi sshd[1871]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
Dec 06 12:00:33 mathis.tp1.cesi sshd[2053]: Accepted password for mathis from 10.1.1.1 port 64520 ssh2
Dec 06 12:00:33 mathis.tp1.cesi sshd[2053]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
lines 1-21/21 (END)...skipping...
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-06 11:34:31 CET; 26min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 856 (sshd)
    Tasks: 1 (limit: 4943)
   Memory: 4.2M
   CGroup: /system.slice/sshd.service
           └─856 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@op>

Dec 06 11:34:31 localhost.localdomain sshd[856]: Server listening on :: port 22.
Dec 06 11:34:31 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
Dec 06 11:47:07 localhost.localdomain sshd[1813]: Accepted password for mathis from 10.1.1.1 port 55608 ssh2
Dec 06 11:47:07 localhost.localdomain sshd[1813]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
Dec 06 11:49:04 mathis.tp1.cesi sshd[1871]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=1>
Dec 06 11:49:06 mathis.tp1.cesi sshd[1871]: Failed password for mathis from 10.1.1.1 port 55658 ssh2
Dec 06 11:49:09 mathis.tp1.cesi sshd[1871]: Accepted password for mathis from 10.1.1.1 port 55658 ssh2
Dec 06 11:49:09 mathis.tp1.cesi sshd[1871]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
Dec 06 12:00:33 mathis.tp1.cesi sshd[2053]: Accepted password for mathis from 10.1.1.1 port 64520 ssh2
Dec 06 12:00:33 mathis.tp1.cesi sshd[2053]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~

[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-06 11:34:31 CET; 26min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 856 (sshd)
    Tasks: 1 (limit: 4943)
   Memory: 4.2M
   CGroup: /system.slice/sshd.service
           └─856 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@op>

Dec 06 11:34:31 localhost.localdomain sshd[856]: Server listening on :: port 22.
Dec 06 11:34:31 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
Dec 06 11:47:07 localhost.localdomain sshd[1813]: Accepted password for mathis from 10.1.1.1 port 55608 ssh2
Dec 06 11:47:07 localhost.localdomain sshd[1813]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
Dec 06 11:49:04 mathis.tp1.cesi sshd[1871]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=1>
Dec 06 11:49:06 mathis.tp1.cesi sshd[1871]: Failed password for mathis from 10.1.1.1 port 55658 ssh2
Dec 06 11:49:09 mathis.tp1.cesi sshd[1871]: Accepted password for mathis from 10.1.1.1 port 55658 ssh2
Dec 06 11:49:09 mathis.tp1.cesi sshd[1871]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
Dec 06 12:00:33 mathis.tp1.cesi sshd[2053]: Accepted password for mathis from 10.1.1.1 port 64520 ssh2
Dec 06 12:00:33 mathis.tp1.cesi sshd[2053]: pam_unix(sshd:session): session opened for user mathis by (uid=0)
lines 1-21/21 (END)

[mathis@mathis ~]$ sudo ss-lutpn
[sudo] password for mathis:
sudo: ss-lutpn: command not found
[mathis@mathis ~]$ netstat -utnp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0     36 10.1.1.2:22             10.1.1.1:64520          ESTABLISHED -
udp        0      0 10.0.2.15:68            10.0.2.2:67             ESTABLISHED -
[mathis@mathis ~]$ ps
    PID TTY          TIME CMD
   2058 pts/0    00:00:00 bash
   2107 pts/0    00:00:00 ps
[mathis@mathis ~]$ sudo vi /etc/ssh/ssh
ssh_config                sshd_config               ssh_host_ecdsa_key.pub    ssh_host_ed25519_key.pub  ssh_host_rsa_key.pub
ssh_config.d/             ssh_host_ecdsa_key        ssh_host_ed25519_key      ssh_host_rsa_key
[mathis@mathis ~]$ sudo vi /etc/ssh/ssh
ssh_config                sshd_config               ssh_host_ecdsa_key.pub    ssh_host_ed25519_key.pub  ssh_host_rsa_key.pub
ssh_config.d/             ssh_host_ecdsa_key        ssh_host_ed25519_key      ssh_host_rsa_key
[mathis@mathis ~]$ sudo vi /etc/ssh/sshd_config
[mathis@mathis ~]$ systemctl restart sshd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to restart 'sshd.service'.
Multiple identities can be used for authentication:
 1.  mathis
 2.  user1
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ systemctl restart sshd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to restart 'sshd.service'.
Multiple identities can be used for authentication:
 1.  mathis
 2.  user1
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
[mathis@mathis ~]$
[mathis@mathis ~]$ netstat -utnp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0     36 10.1.1.2:22             10.1.1.1:64520          ESTABLISHED -
udp        0      0 10.0.2.15:68            10.0.2.2:67             ESTABLISHED -
[mathis@mathis ~]$ exit
logout
Connection to 10.1.1.2 closed.
PS C:\Windows\system32> ssh mathis@10.1.1.2:107
ssh: Could not resolve hostname 10.1.1.2:107: H\364te inconnu.
PS C:\Windows\system32> ssh mathis@10.1.1.2
ssh: connect to host 10.1.1.2 port 22: Connection refused
PS C:\Windows\system32> ssh mathis@10.1.1.2
ssh: connect to host 10.1.1.2 port 22: Connection refused
PS C:\Windows\system32> ssh mathis@10.1.1.2
ssh: connect to host 10.1.1.2 port 22: Connection refused
PS C:\Windows\system32> ssh mathis@10.1.1.2 -port 107
Bad port 'ort'
PS C:\Windows\system32> ssh mathis@10.1.1.2 -port 107
Bad port 'ort'
PS C:\Windows\system32> ssh mathis@10.1.1.2 -port
Bad port 'ort'
PS C:\Windows\system32> ssh mathis@10.1.1.2 -port 107
Bad port 'ort'
PS C:\Windows\system32> ssh mathis@10.1.1.2 -port 107
Bad port 'ort'
PS C:\Windows\system32> ssh mathis@10.1.1.2 -p 107
mathis@10.1.1.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec  6 12:00:33 2021 from 10.1.1.1
[mathis@mathis ~]$ yum install nginx
Error: This command has to be run with superuser privileges (under the root user on most systems).
[mathis@mathis ~]$ sudo yum install nginx
[sudo] password for mathis:
Rocky Linux 8 - AppStream                                                                             1.1 MB/s | 8.3 MB     00:07
Rocky Linux 8 - BaseOS                                                                                1.2 MB/s | 3.5 MB     00:02
Rocky Linux 8 - Extras                                                                                 12 kB/s |  10 kB     00:00
Dependencies resolved.
======================================================================================================================================
 Package                               Architecture     Version                                             Repository           Size
======================================================================================================================================
Installing:
 nginx                                 x86_64           1:1.14.1-9.module+el8.4.0+542+81547229              appstream           566 k
Installing dependencies:
 fontconfig                            x86_64           2.13.1-4.el8                                        baseos              273 k
 gd                                    x86_64           2.2.5-7.el8                                         appstream           143 k
 jbigkit-libs                          x86_64           2.1-14.el8                                          appstream            54 k
 libX11                                x86_64           1.6.8-5.el8                                         appstream           610 k
 libX11-common                         noarch           1.6.8-5.el8                                         appstream           157 k
 libXau                                x86_64           1.0.9-3.el8                                         appstream            36 k
 libXpm                                x86_64           3.5.12-8.el8                                        appstream            57 k
 libjpeg-turbo                         x86_64           1.5.3-12.el8                                        appstream           156 k
 libtiff                               x86_64           4.0.9-20.el8                                        appstream           187 k
 libwebp                               x86_64           1.0.0-5.el8                                         appstream           271 k
 libxcb                                x86_64           1.13.1-1.el8                                        appstream           228 k
 libxslt                               x86_64           1.1.32-6.el8                                        baseos              249 k
 nginx-all-modules                     noarch           1:1.14.1-9.module+el8.4.0+542+81547229              appstream            22 k
 nginx-filesystem                      noarch           1:1.14.1-9.module+el8.4.0+542+81547229              appstream            23 k
 nginx-mod-http-image-filter           x86_64           1:1.14.1-9.module+el8.4.0+542+81547229              appstream            34 k
 nginx-mod-http-perl                   x86_64           1:1.14.1-9.module+el8.4.0+542+81547229              appstream            45 k
 nginx-mod-http-xslt-filter            x86_64           1:1.14.1-9.module+el8.4.0+542+81547229              appstream            32 k
 nginx-mod-mail                        x86_64           1:1.14.1-9.module+el8.4.0+542+81547229              appstream            63 k
 nginx-mod-stream                      x86_64           1:1.14.1-9.module+el8.4.0+542+81547229              appstream            84 k
 perl-Carp                             noarch           1.42-396.el8                                        baseos               29 k
 perl-Data-Dumper                      x86_64           2.167-399.el8                                       baseos               57 k
 perl-Digest                           noarch           1.17-395.el8                                        appstream            26 k
 perl-Digest-MD5                       x86_64           2.55-396.el8                                        appstream            36 k
 perl-Encode                           x86_64           4:2.97-3.el8                                        baseos              1.5 M
 perl-Errno                            x86_64           1.28-420.el8                                        baseos               75 k
 perl-Exporter                         noarch           5.72-396.el8                                        baseos               33 k
 perl-File-Path                        noarch           2.15-2.el8                                          baseos               37 k
 perl-File-Temp                        noarch           0.230.600-1.el8                                     baseos               62 k
 perl-Getopt-Long                      noarch           1:2.50-4.el8                                        baseos               62 k
 perl-HTTP-Tiny                        noarch           0.074-1.el8                                         baseos               57 k
 perl-IO                               x86_64           1.38-420.el8                                        baseos              141 k
 perl-MIME-Base64                      x86_64           3.15-396.el8                                        baseos               30 k
 perl-Net-SSLeay                       x86_64           1.88-1.module+el8.4.0+512+d4f0fc54                  appstream           378 k
 perl-PathTools                        x86_64           3.74-1.el8                                          baseos               89 k
 perl-Pod-Escapes                      noarch           1:1.07-395.el8                                      baseos               19 k
 perl-Pod-Perldoc                      noarch           3.28-396.el8                                        baseos               85 k
 perl-Pod-Simple                       noarch           1:3.35-395.el8                                      baseos              212 k
 perl-Pod-Usage                        noarch           4:1.69-395.el8                                      baseos               33 k
 perl-Scalar-List-Utils                x86_64           3:1.49-2.el8                                        baseos               67 k
 perl-Socket                           x86_64           4:2.027-3.el8                                       baseos               58 k
 perl-Storable                         x86_64           1:3.11-3.el8                                        baseos               97 k
 perl-Term-ANSIColor                   noarch           4.06-396.el8                                        baseos               45 k
 perl-Term-Cap                         noarch           1.17-395.el8                                        baseos               22 k
 perl-Text-ParseWords                  noarch           3.30-395.el8                                        baseos               17 k
 perl-Text-Tabs+Wrap                   noarch           2013.0523-395.el8                                   baseos               23 k
 perl-Time-Local                       noarch           1:1.280-1.el8                                       baseos               32 k
 perl-URI                              noarch           1.73-3.el8                                          appstream           115 k
 perl-Unicode-Normalize                x86_64           1.25-396.el8                                        baseos               81 k
 perl-constant                         noarch           1.33-396.el8                                        baseos               24 k
 perl-interpreter                      x86_64           4:5.26.3-420.el8                                    baseos              6.3 M
 perl-libnet                           noarch           3.11-3.el8                                          appstream           120 k
 perl-libs                             x86_64           4:5.26.3-420.el8                                    baseos              1.6 M
 perl-macros                           x86_64           4:5.26.3-420.el8                                    baseos               71 k
 perl-parent                           noarch           1:0.237-1.el8                                       baseos               19 k
 perl-podlators                        noarch           4.11-1.el8                                          baseos              117 k
 perl-threads                          x86_64           1:2.21-2.el8                                        baseos               60 k
 perl-threads-shared                   x86_64           1.58-2.el8                                          baseos               47 k
Installing weak dependencies:
 perl-IO-Socket-IP                     noarch           0.39-5.el8                                          appstream            46 k
 perl-IO-Socket-SSL                    noarch           2.066-4.module+el8.4.0+512+d4f0fc54                 appstream           297 k
 perl-Mozilla-CA                       noarch           20160104-7.module+el8.4.0+529+e3b3e624              appstream            14 k
Enabling module streams:
 nginx                                                  1.14
 perl                                                   5.26
 perl-IO-Socket-SSL                                     2.066
 perl-libwww-perl                                       6.34

Transaction Summary
======================================================================================================================================
Install  61 Packages

Total download size: 15 M
Installed size: 45 M
Is this ok [y/N]: y
Downloading Packages:
(1/61): jbigkit-libs-2.1-14.el8.x86_64.rpm                                                            167 kB/s |  54 kB     00:00
(2/61): gd-2.2.5-7.el8.x86_64.rpm                                                                     330 kB/s | 143 kB     00:00
(3/61): libXau-1.0.9-3.el8.x86_64.rpm                                                                 319 kB/s |  36 kB     00:00
(4/61): libX11-common-1.6.8-5.el8.noarch.rpm                                                          614 kB/s | 157 kB     00:00
(5/61): libXpm-3.5.12-8.el8.x86_64.rpm                                                                612 kB/s |  57 kB     00:00
(6/61): libjpeg-turbo-1.5.3-12.el8.x86_64.rpm                                                         634 kB/s | 156 kB     00:00
(7/61): libtiff-4.0.9-20.el8.x86_64.rpm                                                               798 kB/s | 187 kB     00:00
(8/61): libX11-1.6.8-5.el8.x86_64.rpm                                                                 657 kB/s | 610 kB     00:00
(9/61): libxcb-1.13.1-1.el8.x86_64.rpm                                                                807 kB/s | 228 kB     00:00
(10/61): libwebp-1.0.0-5.el8.x86_64.rpm                                                               758 kB/s | 271 kB     00:00
(11/61): nginx-all-modules-1.14.1-9.module+el8.4.0+542+81547229.noarch.rpm                            193 kB/s |  22 kB     00:00
(12/61): nginx-filesystem-1.14.1-9.module+el8.4.0+542+81547229.noarch.rpm                             156 kB/s |  23 kB     00:00
(13/61): nginx-mod-http-image-filter-1.14.1-9.module+el8.4.0+542+81547229.x86_64.rpm                  271 kB/s |  34 kB     00:00
(14/61): nginx-mod-http-perl-1.14.1-9.module+el8.4.0+542+81547229.x86_64.rpm                          398 kB/s |  45 kB     00:00
(15/61): nginx-mod-http-xslt-filter-1.14.1-9.module+el8.4.0+542+81547229.x86_64.rpm                   409 kB/s |  32 kB     00:00
(16/61): nginx-mod-mail-1.14.1-9.module+el8.4.0+542+81547229.x86_64.rpm                               564 kB/s |  63 kB     00:00
(17/61): nginx-1.14.1-9.module+el8.4.0+542+81547229.x86_64.rpm                                        865 kB/s | 566 kB     00:00
(18/61): nginx-mod-stream-1.14.1-9.module+el8.4.0+542+81547229.x86_64.rpm                             553 kB/s |  84 kB     00:00
(19/61): perl-Digest-1.17-395.el8.noarch.rpm                                                          215 kB/s |  26 kB     00:00
(20/61): perl-Digest-MD5-2.55-396.el8.x86_64.rpm                                                      316 kB/s |  36 kB     00:00
(21/61): perl-IO-Socket-IP-0.39-5.el8.noarch.rpm                                                      614 kB/s |  46 kB     00:00
(22/61): perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624.noarch.rpm                             83 kB/s |  14 kB     00:00
(23/61): perl-URI-1.73-3.el8.noarch.rpm                                                               596 kB/s | 115 kB     00:00
(24/61): perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54.noarch.rpm                            724 kB/s | 297 kB     00:00
(25/61): perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86_64.rpm                                752 kB/s | 378 kB     00:00
(26/61): perl-libnet-3.11-3.el8.noarch.rpm                                                            747 kB/s | 120 kB     00:00
(27/61): perl-Carp-1.42-396.el8.noarch.rpm                                                            308 kB/s |  29 kB     00:00
(28/61): perl-Data-Dumper-2.167-399.el8.x86_64.rpm                                                    584 kB/s |  57 kB     00:00
(29/61): fontconfig-2.13.1-4.el8.x86_64.rpm                                                           765 kB/s | 273 kB     00:00
(30/61): libxslt-1.1.32-6.el8.x86_64.rpm                                                              909 kB/s | 249 kB     00:00
(31/61): perl-Exporter-5.72-396.el8.noarch.rpm                                                        340 kB/s |  33 kB     00:00
(32/61): perl-Errno-1.28-420.el8.x86_64.rpm                                                           507 kB/s |  75 kB     00:00
(33/61): perl-File-Path-2.15-2.el8.noarch.rpm                                                         420 kB/s |  37 kB     00:00
(34/61): perl-File-Temp-0.230.600-1.el8.noarch.rpm                                                    513 kB/s |  62 kB     00:00
(35/61): perl-Getopt-Long-2.50-4.el8.noarch.rpm                                                       739 kB/s |  62 kB     00:00
(36/61): perl-HTTP-Tiny-0.074-1.el8.noarch.rpm                                                        652 kB/s |  57 kB     00:00
(37/61): perl-MIME-Base64-3.15-396.el8.x86_64.rpm                                                     275 kB/s |  30 kB     00:00
(38/61): perl-IO-1.38-420.el8.x86_64.rpm                                                              709 kB/s | 141 kB     00:00
(39/61): perl-Pod-Escapes-1.07-395.el8.noarch.rpm                                                     311 kB/s |  19 kB     00:00
(40/61): perl-PathTools-3.74-1.el8.x86_64.rpm                                                         579 kB/s |  89 kB     00:00
(41/61): perl-Pod-Perldoc-3.28-396.el8.noarch.rpm                                                     652 kB/s |  85 kB     00:00
(42/61): perl-Pod-Usage-1.69-395.el8.noarch.rpm                                                       341 kB/s |  33 kB     00:00
(43/61): perl-Pod-Simple-3.35-395.el8.noarch.rpm                                                      969 kB/s | 212 kB     00:00
(44/61): perl-Scalar-List-Utils-1.49-2.el8.x86_64.rpm                                                 535 kB/s |  67 kB     00:00
(45/61): perl-Socket-2.027-3.el8.x86_64.rpm                                                           578 kB/s |  58 kB     00:00
(46/61): perl-Term-ANSIColor-4.06-396.el8.noarch.rpm                                                  419 kB/s |  45 kB     00:00
(47/61): perl-Storable-3.11-3.el8.x86_64.rpm                                                          618 kB/s |  97 kB     00:00
(48/61): perl-Term-Cap-1.17-395.el8.noarch.rpm                                                        248 kB/s |  22 kB     00:00
(49/61): perl-Text-ParseWords-3.30-395.el8.noarch.rpm                                                 238 kB/s |  17 kB     00:00
(50/61): perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch.rpm                                             261 kB/s |  23 kB     00:00
(51/61): perl-Time-Local-1.280-1.el8.noarch.rpm                                                       345 kB/s |  32 kB     00:00
(52/61): perl-constant-1.33-396.el8.noarch.rpm                                                        295 kB/s |  24 kB     00:00
(53/61): perl-Encode-2.97-3.el8.x86_64.rpm                                                            1.0 MB/s | 1.5 MB     00:01
(54/61): perl-Unicode-Normalize-1.25-396.el8.x86_64.rpm                                               537 kB/s |  81 kB     00:00
(55/61): perl-macros-5.26.3-420.el8.x86_64.rpm                                                        619 kB/s |  71 kB     00:00
(56/61): perl-parent-0.237-1.el8.noarch.rpm                                                           264 kB/s |  19 kB     00:00
(57/61): perl-podlators-4.11-1.el8.noarch.rpm                                                         608 kB/s | 117 kB     00:00
(58/61): perl-threads-2.21-2.el8.x86_64.rpm                                                           598 kB/s |  60 kB     00:00
(59/61): perl-threads-shared-1.58-2.el8.x86_64.rpm                                                    353 kB/s |  47 kB     00:00
(60/61): perl-libs-5.26.3-420.el8.x86_64.rpm                                                          930 kB/s | 1.6 MB     00:01
(61/61): perl-interpreter-5.26.3-420.el8.x86_64.rpm                                                   1.1 MB/s | 6.3 MB     00:05
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 1.4 MB/s |  15 MB     00:10
Rocky Linux 8 - AppStream                                                                             1.6 MB/s | 1.6 kB     00:00
Importing GPG key 0x6D745A60:
 Userid     : "Release Engineering <infrastructure@rockylinux.org>"
 Fingerprint: 7051 C470 A929 F454 CEBE 37B7 15AF 5DAC 6D74 5A60
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-rockyofficial
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : libjpeg-turbo-1.5.3-12.el8.x86_64                                                                           1/61
  Installing       : perl-Digest-1.17-395.el8.noarch                                                                             2/61
  Installing       : perl-Digest-MD5-2.55-396.el8.x86_64                                                                         3/61
  Installing       : perl-Data-Dumper-2.167-399.el8.x86_64                                                                       4/61
  Installing       : perl-libnet-3.11-3.el8.noarch                                                                               5/61
  Installing       : perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86_64                                                   6/61
  Installing       : perl-URI-1.73-3.el8.noarch                                                                                  7/61
  Installing       : perl-Pod-Escapes-1:1.07-395.el8.noarch                                                                      8/61
  Installing       : perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624.noarch                                               9/61
  Installing       : perl-IO-Socket-IP-0.39-5.el8.noarch                                                                        10/61
  Installing       : perl-Time-Local-1:1.280-1.el8.noarch                                                                       11/61
  Installing       : perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54.noarch                                              12/61
  Installing       : perl-Term-ANSIColor-4.06-396.el8.noarch                                                                    13/61
  Installing       : perl-Term-Cap-1.17-395.el8.noarch                                                                          14/61
  Installing       : perl-File-Temp-0.230.600-1.el8.noarch                                                                      15/61
  Installing       : perl-Pod-Simple-1:3.35-395.el8.noarch                                                                      16/61
  Installing       : perl-HTTP-Tiny-0.074-1.el8.noarch                                                                          17/61
  Installing       : perl-podlators-4.11-1.el8.noarch                                                                           18/61
  Installing       : perl-Pod-Perldoc-3.28-396.el8.noarch                                                                       19/61
  Installing       : perl-Text-ParseWords-3.30-395.el8.noarch                                                                   20/61
  Installing       : perl-Pod-Usage-4:1.69-395.el8.noarch                                                                       21/61
  Installing       : perl-MIME-Base64-3.15-396.el8.x86_64                                                                       22/61
  Installing       : perl-Storable-1:3.11-3.el8.x86_64                                                                          23/61
  Installing       : perl-Getopt-Long-1:2.50-4.el8.noarch                                                                       24/61
  Installing       : perl-Errno-1.28-420.el8.x86_64                                                                             25/61
  Installing       : perl-Socket-4:2.027-3.el8.x86_64                                                                           26/61
  Installing       : perl-Encode-4:2.97-3.el8.x86_64                                                                            27/61
  Installing       : perl-Carp-1.42-396.el8.noarch                                                                              28/61
  Installing       : perl-Exporter-5.72-396.el8.noarch                                                                          29/61
  Installing       : perl-libs-4:5.26.3-420.el8.x86_64                                                                          30/61
  Installing       : perl-Scalar-List-Utils-3:1.49-2.el8.x86_64                                                                 31/61
  Installing       : perl-parent-1:0.237-1.el8.noarch                                                                           32/61
  Installing       : perl-macros-4:5.26.3-420.el8.x86_64                                                                        33/61
  Installing       : perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch                                                               34/61
  Installing       : perl-Unicode-Normalize-1.25-396.el8.x86_64                                                                 35/61
  Installing       : perl-File-Path-2.15-2.el8.noarch                                                                           36/61
  Installing       : perl-IO-1.38-420.el8.x86_64                                                                                37/61
  Installing       : perl-PathTools-3.74-1.el8.x86_64                                                                           38/61
  Installing       : perl-constant-1.33-396.el8.noarch                                                                          39/61
  Installing       : perl-threads-1:2.21-2.el8.x86_64                                                                           40/61
  Installing       : perl-threads-shared-1.58-2.el8.x86_64                                                                      41/61
  Installing       : perl-interpreter-4:5.26.3-420.el8.x86_64                                                                   42/61
  Installing       : libxslt-1.1.32-6.el8.x86_64                                                                                43/61
  Installing       : fontconfig-2.13.1-4.el8.x86_64                                                                             44/61
  Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                                                                             44/61
  Running scriptlet: nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+81547229.noarch                                             45/61
  Installing       : nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+81547229.noarch                                             45/61
  Installing       : libwebp-1.0.0-5.el8.x86_64                                                                                 46/61
  Installing       : libXau-1.0.9-3.el8.x86_64                                                                                  47/61
  Installing       : libxcb-1.13.1-1.el8.x86_64                                                                                 48/61
  Installing       : libX11-common-1.6.8-5.el8.noarch                                                                           49/61
  Installing       : libX11-1.6.8-5.el8.x86_64                                                                                  50/61
  Installing       : libXpm-3.5.12-8.el8.x86_64                                                                                 51/61
  Installing       : jbigkit-libs-2.1-14.el8.x86_64                                                                             52/61
  Running scriptlet: jbigkit-libs-2.1-14.el8.x86_64                                                                             52/61
  Installing       : libtiff-4.0.9-20.el8.x86_64                                                                                53/61
  Installing       : gd-2.2.5-7.el8.x86_64                                                                                      54/61
  Running scriptlet: gd-2.2.5-7.el8.x86_64                                                                                      54/61
  Installing       : nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                          55/61
  Running scriptlet: nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                          55/61
  Installing       : nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                   56/61
  Running scriptlet: nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                   56/61
  Installing       : nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                               57/61
  Running scriptlet: nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                               57/61
  Installing       : nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                             58/61
  Running scriptlet: nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                             58/61
  Installing       : nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                                        59/61
  Running scriptlet: nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                                        59/61
  Installing       : nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                  60/61
  Running scriptlet: nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                  60/61
  Installing       : nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+81547229.noarch                                            61/61
  Running scriptlet: nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+81547229.noarch                                            61/61
  Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                                                                             61/61
  Verifying        : gd-2.2.5-7.el8.x86_64                                                                                       1/61
  Verifying        : jbigkit-libs-2.1-14.el8.x86_64                                                                              2/61
  Verifying        : libX11-1.6.8-5.el8.x86_64                                                                                   3/61
  Verifying        : libX11-common-1.6.8-5.el8.noarch                                                                            4/61
  Verifying        : libXau-1.0.9-3.el8.x86_64                                                                                   5/61
  Verifying        : libXpm-3.5.12-8.el8.x86_64                                                                                  6/61
  Verifying        : libjpeg-turbo-1.5.3-12.el8.x86_64                                                                           7/61
  Verifying        : libtiff-4.0.9-20.el8.x86_64                                                                                 8/61
  Verifying        : libwebp-1.0.0-5.el8.x86_64                                                                                  9/61
  Verifying        : libxcb-1.13.1-1.el8.x86_64                                                                                 10/61
  Verifying        : nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                                        11/61
  Verifying        : nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+81547229.noarch                                            12/61
  Verifying        : nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+81547229.noarch                                             13/61
  Verifying        : nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                  14/61
  Verifying        : nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                          15/61
  Verifying        : nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                   16/61
  Verifying        : nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                               17/61
  Verifying        : nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64                                             18/61
  Verifying        : perl-Digest-1.17-395.el8.noarch                                                                            19/61
  Verifying        : perl-Digest-MD5-2.55-396.el8.x86_64                                                                        20/61
  Verifying        : perl-IO-Socket-IP-0.39-5.el8.noarch                                                                        21/61
  Verifying        : perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54.noarch                                              22/61
  Verifying        : perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624.noarch                                              23/61
  Verifying        : perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86_64                                                  24/61
  Verifying        : perl-URI-1.73-3.el8.noarch                                                                                 25/61
  Verifying        : perl-libnet-3.11-3.el8.noarch                                                                              26/61
  Verifying        : fontconfig-2.13.1-4.el8.x86_64                                                                             27/61
  Verifying        : libxslt-1.1.32-6.el8.x86_64                                                                                28/61
  Verifying        : perl-Carp-1.42-396.el8.noarch                                                                              29/61
  Verifying        : perl-Data-Dumper-2.167-399.el8.x86_64                                                                      30/61
  Verifying        : perl-Encode-4:2.97-3.el8.x86_64                                                                            31/61
  Verifying        : perl-Errno-1.28-420.el8.x86_64                                                                             32/61
  Verifying        : perl-Exporter-5.72-396.el8.noarch                                                                          33/61
  Verifying        : perl-File-Path-2.15-2.el8.noarch                                                                           34/61
  Verifying        : perl-File-Temp-0.230.600-1.el8.noarch                                                                      35/61
  Verifying        : perl-Getopt-Long-1:2.50-4.el8.noarch                                                                       36/61
  Verifying        : perl-HTTP-Tiny-0.074-1.el8.noarch                                                                          37/61
  Verifying        : perl-IO-1.38-420.el8.x86_64                                                                                38/61
  Verifying        : perl-MIME-Base64-3.15-396.el8.x86_64                                                                       39/61
  Verifying        : perl-PathTools-3.74-1.el8.x86_64                                                                           40/61
  Verifying        : perl-Pod-Escapes-1:1.07-395.el8.noarch                                                                     41/61
  Verifying        : perl-Pod-Perldoc-3.28-396.el8.noarch                                                                       42/61
  Verifying        : perl-Pod-Simple-1:3.35-395.el8.noarch                                                                      43/61
  Verifying        : perl-Pod-Usage-4:1.69-395.el8.noarch                                                                       44/61
  Verifying        : perl-Scalar-List-Utils-3:1.49-2.el8.x86_64                                                                 45/61
  Verifying        : perl-Socket-4:2.027-3.el8.x86_64                                                                           46/61
  Verifying        : perl-Storable-1:3.11-3.el8.x86_64                                                                          47/61
  Verifying        : perl-Term-ANSIColor-4.06-396.el8.noarch                                                                    48/61
  Verifying        : perl-Term-Cap-1.17-395.el8.noarch                                                                          49/61
  Verifying        : perl-Text-ParseWords-3.30-395.el8.noarch                                                                   50/61
  Verifying        : perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch                                                               51/61
  Verifying        : perl-Time-Local-1:1.280-1.el8.noarch                                                                       52/61
  Verifying        : perl-Unicode-Normalize-1.25-396.el8.x86_64                                                                 53/61
  Verifying        : perl-constant-1.33-396.el8.noarch                                                                          54/61
  Verifying        : perl-interpreter-4:5.26.3-420.el8.x86_64                                                                   55/61
  Verifying        : perl-libs-4:5.26.3-420.el8.x86_64                                                                          56/61
  Verifying        : perl-macros-4:5.26.3-420.el8.x86_64                                                                        57/61
  Verifying        : perl-parent-1:0.237-1.el8.noarch                                                                           58/61
  Verifying        : perl-podlators-4.11-1.el8.noarch                                                                           59/61
  Verifying        : perl-threads-1:2.21-2.el8.x86_64                                                                           60/61
  Verifying        : perl-threads-shared-1.58-2.el8.x86_64                                                                      61/61

Installed:
  fontconfig-2.13.1-4.el8.x86_64
  gd-2.2.5-7.el8.x86_64
  jbigkit-libs-2.1-14.el8.x86_64
  libX11-1.6.8-5.el8.x86_64
  libX11-common-1.6.8-5.el8.noarch
  libXau-1.0.9-3.el8.x86_64
  libXpm-3.5.12-8.el8.x86_64
  libjpeg-turbo-1.5.3-12.el8.x86_64
  libtiff-4.0.9-20.el8.x86_64
  libwebp-1.0.0-5.el8.x86_64
  libxcb-1.13.1-1.el8.x86_64
  libxslt-1.1.32-6.el8.x86_64
  nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+81547229.noarch
  nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+81547229.noarch
  nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  perl-Carp-1.42-396.el8.noarch
  perl-Data-Dumper-2.167-399.el8.x86_64
  perl-Digest-1.17-395.el8.noarch
  perl-Digest-MD5-2.55-396.el8.x86_64
  perl-Encode-4:2.97-3.el8.x86_64
  perl-Errno-1.28-420.el8.x86_64
  perl-Exporter-5.72-396.el8.noarch
  perl-File-Path-2.15-2.el8.noarch
  perl-File-Temp-0.230.600-1.el8.noarch
  perl-Getopt-Long-1:2.50-4.el8.noarch
  perl-HTTP-Tiny-0.074-1.el8.noarch
  perl-IO-1.38-420.el8.x86_64
  perl-IO-Socket-IP-0.39-5.el8.noarch
  perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54.noarch
  perl-MIME-Base64-3.15-396.el8.x86_64
  perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624.noarch
  perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86_64
  perl-PathTools-3.74-1.el8.x86_64
  perl-Pod-Escapes-1:1.07-395.el8.noarch
  perl-Pod-Perldoc-3.28-396.el8.noarch
  perl-Pod-Simple-1:3.35-395.el8.noarch
  perl-Pod-Usage-4:1.69-395.el8.noarch
  perl-Scalar-List-Utils-3:1.49-2.el8.x86_64
  perl-Socket-4:2.027-3.el8.x86_64
  perl-Storable-1:3.11-3.el8.x86_64
  perl-Term-ANSIColor-4.06-396.el8.noarch
  perl-Term-Cap-1.17-395.el8.noarch
  perl-Text-ParseWords-3.30-395.el8.noarch
  perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch
  perl-Time-Local-1:1.280-1.el8.noarch
  perl-URI-1.73-3.el8.noarch
  perl-Unicode-Normalize-1.25-396.el8.x86_64
  perl-constant-1.33-396.el8.noarch
  perl-interpreter-4:5.26.3-420.el8.x86_64
  perl-libnet-3.11-3.el8.noarch
  perl-libs-4:5.26.3-420.el8.x86_64
  perl-macros-4:5.26.3-420.el8.x86_64
  perl-parent-1:0.237-1.el8.noarch
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64

Complete!
[mathis@mathis ~]$ sudo systemctl start nginx
[mathis@mathis ~]$ netstat -utpn
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0     36 10.1.1.2:107            10.1.1.1:58206          ESTABLISHED -
udp        0      0 10.0.2.15:68            10.0.2.2:67             ESTABLISHED -
[mathis@mathis ~]$ firewall-cmd --permanent --add-port=80/tcp
Authorization failed.
    Make sure polkit agent is running or run the application as superuser.
[mathis@mathis ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[mathis@mathis ~]$ sudo firewall-cmd --reload
success
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ curl
curl: try 'curl --help' or 'curl --manual' for more information
[mathis@mathis ~]$ curl 10.1.1.23
^C
[mathis@mathis ~]$ curl 10.1.1.2
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        background-color: #fff;
        color: #000;
        font-size: 0.9em;
        font-family: sans-serif, helvetica;
        margin: 0;
        padding: 0;
      }
      :link {
        color: #c00;
      }
      :visited {
        color: #c00;
      }
      a:hover {
        color: #f50;
      }
      h1 {
        text-align: center;
        margin: 0;
        padding: 0.6em 2em 0.4em;
        background-color: #10B981;
        color: #fff;
        font-weight: normal;
        font-size: 1.75em;
        border-bottom: 2px solid #000;
      }
      h1 strong {
        font-weight: bold;
        font-size: 1.5em;
      }
      h2 {
        text-align: center;
        background-color: #10B981;
        font-size: 1.1em;
        font-weight: bold;
        color: #fff;
        margin: 0;
        padding: 0.5em;
        border-bottom: 2px solid #000;
      }
      hr {
        display: none;
      }
      .content {
        padding: 1em 5em;
      }
      .alert {
        border: 2px solid #000;
      }

      img {
        border: 2px solid #fff;
        padding: 2px;
        margin: 2px;
      }
      a:hover img {
        border: 2px solid #294172;
      }
      .logos {
        margin: 1em;
        text-align: center;
      }
      /*]]>*/
    </style>
  </head>

  <body>
    <h1>Welcome to <strong>nginx</strong> on Rocky Linux!</h1>

    <div class="content">
      <p>
        This page is used to test the proper operation of the
        <strong>nginx</strong> HTTP server after it has been installed. If you
        can read this page, it means that the web server installed at this site
        is working properly.
      </p>

      <div class="alert">
        <h2>Website Administrator</h2>
        <div class="content">
          <p>
            This is the default <tt>index.html</tt> page that is distributed
            with <strong>nginx</strong> on Rocky Linux. It is located in
            <tt>/usr/share/nginx/html</tt>.
          </p>

          <p>
            You should now put your content in a location of your choice and
            edit the <tt>root</tt> configuration directive in the
            <strong>nginx</strong>
            configuration file
            <tt>/etc/nginx/nginx.conf</tt>.
          </p>

          <p>
            For information on Rocky Linux, please visit the
            <a href="https://www.rockylinux.org/">Rocky Linux website</a>. The
            documentation for Rocky Linux is
            <a href="https://www.rockylinux.org/"
              >available on the Rocky Linux website</a
            >.
          </p>
        </div>
      </div>

      <div class="logos">
        <a href="http://nginx.net/"
          ><img
            src="nginx-logo.png"
            alt="[ Powered by nginx ]"
            width="121"
            height="32"
        /></a>
        <a href="http://www.rockylinux.org/"><img
            src="poweredby.png"
            alt="[ Powered by Rocky Linux ]"
            width="88" height="31" /></a>

      </div>
    </div>
  </body>
</html>
[mathis@mathis ~]$
[mathis@mathis ~]$ python -m http.server 8888
-bash: python: command not found
[mathis@mathis ~]$ python3 -m http.server 8888
-bash: python3: command not found
[mathis@mathis ~]$ yum install python3 -y
Error: This command has to be run with superuser privileges (under the root user on most systems).
[mathis@mathis ~]$ sudo yum install python3 -y
Last metadata expiration check: 0:05:03 ago on Mon 06 Dec 2021 12:07:13 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                         Architecture        Version                                             Repository              Size
======================================================================================================================================
Installing:
 python36                        x86_64              3.6.8-38.module+el8.5.0+671+195e4563                appstream               18 k
Installing dependencies:
 python3-pip                     noarch              9.0.3-20.el8.rocky.0                                appstream               19 k
 python3-setuptools              noarch              39.2.0-6.el8                                        baseos                 162 k
Enabling module streams:
 python36                                            3.6

Transaction Summary
======================================================================================================================================
Install  3 Packages

Total download size: 199 k
Installed size: 466 k
Downloading Packages:
(1/3): python3-pip-9.0.3-20.el8.rocky.0.noarch.rpm                                                     90 kB/s |  19 kB     00:00
(2/3): python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64.rpm                                        86 kB/s |  18 kB     00:00
(3/3): python3-setuptools-39.2.0-6.el8.noarch.rpm                                                     434 kB/s | 162 kB     00:00
--------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                 148 kB/s | 199 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                              1/1
  Installing       : python3-setuptools-39.2.0-6.el8.noarch                                                                       1/3
  Installing       : python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64                                                         2/3
  Running scriptlet: python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64                                                         2/3
  Installing       : python3-pip-9.0.3-20.el8.rocky.0.noarch                                                                      3/3
  Running scriptlet: python3-pip-9.0.3-20.el8.rocky.0.noarch                                                                      3/3
  Verifying        : python3-pip-9.0.3-20.el8.rocky.0.noarch                                                                      1/3
  Verifying        : python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64                                                         2/3
  Verifying        : python3-setuptools-39.2.0-6.el8.noarch                                                                       3/3

Installed:
  python3-pip-9.0.3-20.el8.rocky.0.noarch python3-setuptools-39.2.0-6.el8.noarch python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64

Complete!
[mathis@mathis ~]$ python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
^C
Keyboard interrupt received, exiting.
[mathis@mathis ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
Warning: ALREADY_ENABLED: 80:tcp
success
[mathis@mathis ~]$ ^C
[mathis@mathis ~]$ sudo firewall-cmd --permanent --add-port=8888/tcp
success
[mathis@mathis ~]$ sudo firewall-cmd --reload
success
[mathis@mathis ~]$ python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
10.1.1.1 - - [06/Dec/2021 12:13:19] "GET / HTTP/1.1" 200 -
10.1.1.1 - - [06/Dec/2021 12:13:19] code 404, message File not found
10.1.1.1 - - [06/Dec/2021 12:13:19] "GET /favicon.ico HTTP/1.1" 404 -
^C
Keyboard interrupt received, exiting.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/toto.service
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ [mathis@mathis ~]$ systemctl daemon-reload
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Multiple identities can be used for authentication:
 1.  mathis
 2.  user1
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 12:16:34 CET; 2s ago
  Process: 27073 ExecStart=/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27073 (code=exited, status=2)

Dec 06 12:16:34 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 12:16:34 mathis.tp1.cesi python3[27073]: /bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 12:16:34 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 12:16:34 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl start web.service
Warning: The unit file, source configuration file or drop-ins of web.service changed on disk. Run 'systemctl daemon-reload' to reload units.
[mathis@mathis ~]$ systemctl daemon-reload
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Multiple identities can be used for authentication:
 1.  mathis
 2.  user1
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 12:17:41 CET; 3s ago
  Process: 27130 ExecStart=/usr/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27130 (code=exited, status=2)

Dec 06 12:17:41 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 12:17:41 mathis.tp1.cesi python3[27130]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 12:17:41 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 12:17:41 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ which python
/usr/bin/which: no python in (/home/mathis/.local/bin:/home/mathis/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin)
[mathis@mathis ~]$ which python3
/usr/bin/python3
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl start web.service
Warning: The unit file, source configuration file or drop-ins of web.service changed on disk. Run 'systemctl daemon-reload' to reload units.
[mathis@mathis ~]$ systemctl daemon-reload
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Multiple identities can be used for authentication:
 1.  mathis
 2.  user1
Choose identity to authenticate as (1-2): 13
Invalid response `13'.
==== AUTHENTICATION CANCELED ====
Failed to reload daemon: Access denied
[mathis@mathis ~]$ toto
-bash: toto: command not found
[mathis@mathis ~]$ systemctl daemon-reload
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Multiple identities can be used for authentication:
 1.  mathis
 2.  user1
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
[mathis@mathis ~]$ sudo systemctl start web.service
Failed to start web.service: Unit web.service has a bad unit file setting.
See system logs and 'systemctl status web.service' for details.
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: bad-setting (Reason: Unit web.service has a bad unit file setting.)
   Active: failed (Result: exit-code) since Mon 2021-12-06 12:19:03 CET; 21s ago
 Main PID: 27158 (code=exited, status=2)

Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 12:19:03 mathis.tp1.cesi python3[27158]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
Dec 06 12:19:16 mathis.tp1.cesi systemd[1]: /etc/systemd/system/web.service:5: Neither a valid executable name nor an absolute path: >

[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[sudo] password for mathis:
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: bad-setting (Reason: Unit web.service has a bad unit file setting.)
   Active: failed (Result: exit-code) since Mon 2021-12-06 12:19:03 CET; 57min ago
 Main PID: 27158 (code=exited, status=2)

Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 12:19:03 mathis.tp1.cesi python3[27158]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
Dec 06 12:19:16 mathis.tp1.cesi systemd[1]: /etc/systemd/system/web.service:5: Neither a valid executable name nor an absolute path: >
lines 1-10/10 (END)
sudo vi /etc/systemdystemctl daemon-reload
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Multiple identities can be used for authentication:
 1.  mathis
 2.  user1
Choose identity to authenticate as (1-2): toto
Invalid response `toto'.
==== AUTHENTICATION CANCELED ====
Failed to reload daemon: Access denied
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
Failed to start web.service: Unit web.service has a bad unit file setting.
See system logs and 'systemctl status web.service' for details.
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: bad-setting (Reason: Unit web.service has a bad unit file setting.)
   Active: failed (Result: exit-code) since Mon 2021-12-06 12:19:03 CET; 58min ago
 Main PID: 27158 (code=exited, status=2)

Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 12:19:03 mathis.tp1.cesi python3[27158]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
Dec 06 12:19:16 mathis.tp1.cesi systemd[1]: /etc/systemd/system/web.service:5: Neither a valid executable name nor an absolute path: >
Dec 06 13:16:48 mathis.tp1.cesi systemd[1]: /etc/systemd/system/web.service:5: Neither a valid executable name nor an absolute path: >
lines 1-11/11 (END)

[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
Failed to start web.service: Unit web.service has a bad unit file setting.
See system logs and 'systemctl status web.service' for details.
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: bad-setting (Reason: Unit web.service has a bad unit file setting.)
   Active: failed (Result: exit-code) since Mon 2021-12-06 12:19:03 CET; 58min ago
 Main PID: 27158 (code=exited, status=2)

Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 12:19:03 mathis.tp1.cesi python3[27158]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 12:19:03 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
Dec 06 12:19:16 mathis.tp1.cesi systemd[1]: /etc/systemd/system/web.service:5: Neither a valid executable name nor an absolute path: >
Dec 06 13:16:48 mathis.tp1.cesi systemd[1]: /etc/systemd/system/web.service:5: Neither a valid executable name nor an absolute path: >
Dec 06 13:17:17 mathis.tp1.cesi systemd[1]: /etc/systemd/system/web.service:5: Neither a valid executable name nor an absolute path: >
lines 1-12/12 (END)

[mathis@mathis ~]$ which py
pydoc-3     pydoc3      pydoc3.6    python3     python3.6   python3.6m  pyvenv-3    pyvenv-3.6
[mathis@mathis ~]$ which python3
/usr/bin/python3
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 13:17:56 CET; 3s ago
  Process: 27491 ExecStart=/usr/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27491 (code=exited, status=2)

Dec 06 13:17:56 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 13:17:56 mathis.tp1.cesi python3[27491]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 13:17:56 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 13:17:56 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl reload web.service
Failed to reload web.service: Job type reload is not applicable for unit web.service.
[mathis@mathis ~]$ sudo systemctl start web.service
Warning: The unit file, source configuration file or drop-ins of web.service changed on disk. Run 'systemctl daemon-reload' to reload units.
[mathis@mathis ~]$ sudo systemctl d
daemon-reexec  daemon-reload  default        disable
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 13:20:01 CET; 7s ago
  Process: 27544 ExecStart=/usr/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27544 (code=exited, status=2)

Dec 06 13:20:01 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 13:20:01 mathis.tp1.cesi python3[27544]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 13:20:01 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 13:20:01 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ ls /usr/bin/python3
/usr/bin/python3
[mathis@mathis ~]$ ll
total 0
[mathis@mathis ~]$ ll /usr/bin/python3
lrwxrwxrwx. 1 root root 25 Dec  6 12:12 /usr/bin/python3 -> /etc/alternatives/python3
[mathis@mathis ~]$ which python3
/usr/bin/python3
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ /usr/bin/python3 http.server 8888
/usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
[mathis@mathis ~]$ ^C
[mathis@mathis ~]$ /usr/bin/python3 http.server 8888^C
[mathis@mathis ~]$ python3
Python 3.6.8 (default, Nov  9 2021, 14:44:26)
[GCC 8.5.0 20210514 (Red Hat 8.5.0-3)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
KeyboardInterrupt
>>> exit
Use exit() or Ctrl-D (i.e. EOF) to exit
>>>
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ which python3
/usr/bin/python3
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ [mathis@mathis ~]$
[mathis@mathis ~]$ useradd web
useradd: Permission denied.
useradd: cannot lock /etc/passwd; try again later.
[mathis@mathis ~]$ sudo useradd web
[mathis@mathis ~]$ mkdir /srv/web/
mkdir: cannot create directory ‘/srv/web/’: Permission denied
[mathis@mathis ~]$ sudo mkdir /srv/web/
[mathis@mathis ~]$ touch /srv/web/index
touch: cannot touch '/srv/web/index': Permission denied
[mathis@mathis ~]$ sudo touch /srv/web/index
[mathis@mathis ~]$ sudo vi /srv/web/index
[mathis@mathis ~]$ sudo vi /srv/web/index
[mathis@mathis ~]$ chown /srv/web/index web
chown: invalid user: ‘/srv/web/index’
[mathis@mathis ~]$ chown web /srv/web/index
chown: changing ownership of '/srv/web/index': Operation not permitted
[mathis@mathis ~]$ sudo chown web /srv/web/index
[mathis@mathis ~]$ ll /srv/web/index
-rw-r--r--. 1 web root 19 Dec  6 13:31 /srv/web/index
[mathis@mathis ~]$ cd /srv/web/
[mathis@mathis web]$ ls -al
total 4
drwxr-xr-x. 2 root root 19 Dec  6 13:31 .
drwxr-xr-x. 3 root root 17 Dec  6 13:30 ..
-rw-r--r--. 1 web  root 19 Dec  6 13:31 index
[mathis@mathis web]$ cd
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[sudo] password for mathis:
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systmeclt start web
sudo: systmeclt: command not found
[mathis@mathis ~]$ sudo systmectl start web
sudo: systmectl: command not found
[mathis@mathis ~]$ sudo systmectl start web.service$
sudo: systmectl: command not found
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 13:39:27 CET; 4s ago
  Process: 27694 ExecStart=/usr/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27694 (code=exited, status=2)

Dec 06 13:39:27 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 13:39:27 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 13:39:27 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl start web.service
Warning: The unit file, source configuration file or drop-ins of web.service changed on disk. Run 'systemctl daemon-reload' to reload units.
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 13:43:51 CET; 3s ago
  Process: 27737 ExecStart=/usr/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27737 (code=exited, status=2)

Dec 06 13:43:51 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 13:43:51 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 13:43:51 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 13:44:40 CET; 2s ago
  Process: 27772 ExecStart=/usr/bin/python3 http.server 8888; (code=exited, status=217/USER)
 Main PID: 27772 (code=exited, status=217/USER)

Dec 06 13:44:40 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 13:44:40 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=217/USER
Dec 06 13:44:40 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 13:46:07 CET; 1s ago
  Process: 27806 ExecStart=/usr/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27806 (code=exited, status=2)

Dec 06 13:46:07 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 13:46:07 mathis.tp1.cesi python3[27806]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 13:46:07 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 13:46:07 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ which python3
/usr/bin/python3
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Mon 2021-12-06 13:46:07 CET; 2min 0s ago
  Process: 27806 ExecStart=/usr/bin/python3 http.server 8888 (code=exited, status=2)
 Main PID: 27806 (code=exited, status=2)

Dec 06 13:46:07 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
Dec 06 13:46:07 mathis.tp1.cesi python3[27806]: /usr/bin/python3: can't open file 'http.server': [Errno 2] No such file or directory
Dec 06 13:46:07 mathis.tp1.cesi systemd[1]: web.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Dec 06 13:46:07 mathis.tp1.cesi systemd[1]: web.service: Failed with result 'exit-code'.
[mathis@mathis ~]$ /usr/bin/python3
Python 3.6.8 (default, Nov  9 2021, 14:44:26)
[GCC 8.5.0 20210514 (Red Hat 8.5.0-3)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
KeyboardInterrupt
>>> exit n
KeyboardInterrupt
>>>
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ /usr/bin/python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
^C
Keyboard interrupt received, exiting.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[sudo] password for mathis:
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-06 13:54:38 CET; 2s ago
 Main PID: 27853 (python3)
    Tasks: 1 (limit: 4943)
   Memory: 9.4M
   CGroup: /system.slice/web.service
           └─27853 /usr/bin/python3 -m http.server 8888

Dec 06 13:54:38 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
[mathis@mathis ~]$ ^C
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$ sudo systemctl daemon-reload
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl stop web.service
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl start web.service
[mathis@mathis ~]$ sudo systemctl status web.service
● web.service - <DESCRIPTION>
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-06 13:55:22 CET; 9s ago
 Main PID: 27895 (python3)
    Tasks: 1 (limit: 4943)
   Memory: 9.4M
   CGroup: /system.slice/web.service
           └─27895 /usr/bin/python3 -m http.server 8888

Dec 06 13:55:22 mathis.tp1.cesi systemd[1]: Started <DESCRIPTION>.
[mathis@mathis ~]$ sudo vi /etc/systemd/system/web.service
[mathis@mathis ~]$
[mathis@mathis ~]$
[mathis@mathis ~]$ sudo systemctl stop web.service
[mathis@mathis ~]$ sudo systemctl start web.service
``` 
