###Sécurisation SSH
```bash
PS C:\Users\jeann> ssh mathis@10.2.1.11 -p 107 -i C:\Users\jeann\.ssh\id_rsa_web_tp2
Enter passphrase for key 'C:\Users\jeann\.ssh\id_rsa_web_tp2':
Activate the web console with: systemctl enable --now cockpit.socket

Last failed login: Wed Dec  8 09:53:00 CET 2021 from 10.2.1.12 on ssh:notty
There were 21 failed login attempts since the last successful login.
Last login: Wed Dec  8 09:33:11 2021 from 10.2.1.1
[mathis@web ~]$
``` 
###NGINX
``` bash 
 server {
    listen 80;
    server_name web.tp2.cesi;

    location / {
        proxy_pass http://web.tp2.cesi;
        #proxy_http_version  1.1;
        #proxy_cache_bypass  $http_upgrade;

        #proxy_set_header Upgrade           $http_upgrade;
        #proxy_set_header Connection        "upgrade";
        #proxy_set_header Host              $host;
        #proxy_set_header X-Real-IP         $remote_addr;
        #proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        #proxy_set_header X-Forwarded-Proto $scheme;
        #proxy_set_header X-Forwarded-Host  $host;
        #proxy_set_header X-Forwarded-Port  $server_port;
  }

}

```
###HTTPS
```bash 
[mathis@proxy ~]$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout tp2.cesi.key -out tp2.cesi.crt
Generating a RSA private key
....................................................................................................................................................................................++++
...................................................................................++++
writing new private key to 'tp2.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Bordeaux
Locality Name (eg, city) [Default City]:Bordeaux city
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:RISR
Common Name (eg, your name or your server's hostname) []:tp2.cesi
Email Address []:mathis.jeannot@viacesi.fr
sudo mv tp2.cesi.key /etc/pki/tls/private/
sudo mv tp2.cesi.crt /etc/pki/tls/certs/
```

``` bash
[mathis@proxy ~]$ curl -k 10.2.1.11/index.php
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 data-requesttoken="NOiDE4SwggsvAPS9NWy7cnhevcJNCop6zAdqdQ0p0sw=:cZrLK9T/4U9oM4foRV+UJRYv5ZoiY709nGpYGSJD+YY=">
                <meta charset="utf-8">
                <title>
                Nextcloud               </title>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
                                <meta name="apple-itunes-app" content="app-id=1125420102">
                                <meta name="theme-color" content="#0082c9">
                <link rel="icon" href="/core/img/favicon.ico">
                <link rel="apple-touch-icon" href="/core/img/favicon-touch.png">
                <link rel="mask-icon" sizes="any" href="/core/img/favicon-mask.svg" color="#0082c9">
                <link rel="manifest" href="/core/img/manifest.json">
                <link rel="stylesheet" href="/core/css/guest.css?v=27b925b1-0">
                <script nonce="Tk9pREU0U3dnZ3N2QVBTOU5XeTdjbmhldmNKTkNvcDZ6QWRxZFEwcDBzdz06Y1pyTEs5VC80VTlvTTRmb1JWK1VKUll2NVpvaVk3MDluR3BZR1NKRCtZWT0=" defer src="/index.php/core/js/oc.js?v=27b925b1"></script>
<script nonce="Tk9pREU0U3dnZ3N2QVBTOU5XeTdjbmhldmNKTkNvcDZ6QWRxZFEwcDBzdz06Y1pyTEs5VC80VTlvTTRmb1JWK1VKUll2NVpvaVk3MDluR3BZR1NKRCtZWT0=" defer src="/core/js/dist/main.js?v=27b925b1-0"></script>
<script nonce="Tk9pREU0U3dnZ3N2QVBTOU5XeTdjbmhldmNKTkNvcDZ6QWRxZFEwcDBzdz06Y1pyTEs5VC80VTlvTTRmb1JWK1VKUll2NVpvaVk3MDluR3BZR1NKRCtZWT0=" defer src="/core/js/dist/files_fileinfo.js?v=27b925b1-0"></script>
<script nonce="Tk9pREU0U3dnZ3N2QVBTOU5XeTdjbmhldmNKTkNvcDZ6QWRxZFEwcDBzdz06Y1pyTEs5VC80VTlvTTRmb1JWK1VKUll2NVpvaVk3MDluR3BZR1NKRCtZWT0=" defer src="/core/js/dist/files_client.js?v=27b925b1-0"></script>
<script nonce="Tk9pREU0U3dnZ3N2QVBTOU5XeTdjbmhldmNKTkNvcDZ6QWRxZFEwcDBzdz06Y1pyTEs5VC80VTlvTTRmb1JWK1VKUll2NVpvaVk3MDluR3BZR1NKRCtZWT0=" defer src="/index.php/js/core/merged-template-prepend.js?v=27b925b1-0"></script>
<script nonce="Tk9pREU0U3dnZ3N2QVBTOU5XeTdjbmhldmNKTkNvcDZ6QWRxZFEwcDBzdz06Y1pyTEs5VC80VTlvTTRmb1JWK1VKUll2NVpvaVk3MDluR3BZR1NKRCtZWT0=" defer src="/core/js/backgroundjobs.js?v=27b925b1-0"></script>
                        </head>
        <body id="body-login">
                <noscript>
        <div id="nojavascript">
                <div>
                        This application requires JavaScript for correct operation. Please <a href="https://www.enable-javascript.com/" target="_blank" rel="noreferrer noopener">enable JavaScript</a> and reload the page.          </div>
        </div>
</noscript>
                                        <input type="hidden" id="initial-state-core-config" value="eyJzZXNzaW9uX2xpZmV0aW1lIjoxNDQwLCJzZXNzaW9uX2tlZXBhbGl2ZSI6dHJ1ZSwiYXV0b19sb2dvdXQiOmZhbHNlLCJ2ZXJzaW9uIjoiMjEuMC4xLjEiLCJ2ZXJzaW9uc3RyaW5nIjoiMjEuMC4xIiwiZW5hYmxlX2F2YXRhcnMiOnRydWUsImxvc3RfcGFzc3dvcmRfbGluayI6bnVsbCwibW9kUmV3cml0ZVdvcmtpbmciOmZhbHNlLCJzaGFyaW5nLm1heEF1dG9jb21wbGV0ZVJlc3VsdHMiOjI1LCJzaGFyaW5nLm1pblNlYXJjaFN0cmluZ0xlbmd0aCI6MCwiYmxhY2tsaXN0X2ZpbGVzX3JlZ2V4IjoiXFwuKHBhcnR8ZmlsZXBhcnQpJCJ9">
                                        <input type="hidden" id="initial-state-core-capabilities" value="eyJjb3JlIjp7InBvbGxpbnRlcnZhbCI6NjAsIndlYmRhdi1yb290IjoicmVtb3RlLnBocFwvd2ViZGF2In0sImJydXRlZm9yY2UiOnsiZGVsYXkiOjB9LCJmaWxlcyI6eyJiaWdmaWxlY2h1bmtpbmciOnRydWUsImJsYWNrbGlzdGVkX2ZpbGVzIjpbIi5odGFjY2VzcyJdLCJkaXJlY3RFZGl0aW5nIjp7InVybCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvb2NzXC92Mi5waHBcL2FwcHNcL2ZpbGVzXC9hcGlcL3YxXC9kaXJlY3RFZGl0aW5nIiwiZXRhZyI6IjYyMjZiYTg3MzM3M2Y1ZTczYTNlZjUwNDEwNzUyM2Y3In0sImNvbW1lbnRzIjp0cnVlLCJ1bmRlbGV0ZSI6dHJ1ZSwidmVyc2lvbmluZyI6dHJ1ZX0sImFjdGl2aXR5Ijp7ImFwaXYyIjpbImZpbHRlcnMiLCJmaWx0ZXJzLWFwaSIsInByZXZpZXdzIiwicmljaC1zdHJpbmdzIl19LCJvY20iOnsiZW5hYmxlZCI6dHJ1ZSwiYXBpVmVyc2lvbiI6IjEuMC1wcm9wb3NhbDEiLCJlbmRQb2ludCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvaW5kZXgucGhwXC9vY20iLCJyZXNvdXJjZVR5cGVzIjpbeyJuYW1lIjoiZmlsZSIsInNoYXJlVHlwZXMiOlsidXNlciIsImdyb3VwIl0sInByb3RvY29scyI6eyJ3ZWJkYXYiOiJcL3B1YmxpYy5waHBcL3dlYmRhdlwvIn19XX0sImRhdiI6eyJjaHVua2luZyI6IjEuMCJ9LCJub3RpZmljYXRpb25zIjp7Im9jcy1lbmRwb2ludHMiOlsibGlzdCIsImdldCIsImRlbGV0ZSIsImRlbGV0ZS1hbGwiLCJpY29ucyIsInJpY2gtc3RyaW5ncyIsImFjdGlvbi13ZWIiLCJ1c2VyLXN0YXR1cyJdLCJwdXNoIjpbImRldmljZXMiLCJvYmplY3QtZGF0YSIsImRlbGV0ZSJdLCJhZG1pbi1ub3RpZmljYXRpb25zIjpbIm9jcyIsImNsaSJdfSwicGFzc3dvcmRfcG9saWN5Ijp7Im1pbkxlbmd0aCI6OCwiZW5mb3JjZU5vbkNvbW1vblBhc3N3b3JkIjp0cnVlLCJlbmZvcmNlTnVtZXJpY0NoYXJhY3RlcnMiOmZhbHNlLCJlbmZvcmNlU3BlY2lhbENoYXJhY3RlcnMiOmZhbHNlLCJlbmZvcmNlVXBwZXJMb3dlckNhc2UiOmZhbHNlLCJhcGkiOnsiZ2VuZXJhdGUiOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL29jc1wvdjIucGhwIiwidmFsaWRhdGUiOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL29jc1wvdjIucGhwIn19LCJwcm92aXNpb25pbmdfYXBpIjp7InZlcnNpb24iOiIxLjExLjAiLCJBY2NvdW50UHJvcGVydHlTY29wZXNWZXJzaW9uIjoyLCJBY2NvdW50UHJvcGVydHlTY29wZXNGZWRlcmF0aW9uRW5hYmxlZCI6dHJ1ZX0sImZpbGVzX3NoYXJpbmciOnsic2hhcmVieW1haWwiOnsiZW5hYmxlZCI6dHJ1ZSwidXBsb2FkX2ZpbGVzX2Ryb3AiOnsiZW5hYmxlZCI6dHJ1ZX0sInBhc3N3b3JkIjp7ImVuYWJsZWQiOnRydWUsImVuZm9yY2VkIjpmYWxzZX0sImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwiYXBpX2VuYWJsZWQiOnRydWUsInB1YmxpYyI6eyJlbmFibGVkIjp0cnVlLCJwYXNzd29yZCI6eyJlbmZvcmNlZCI6ZmFsc2UsImFza0Zvck9wdGlvbmFsUGFzc3dvcmQiOmZhbHNlfSwiZXhwaXJlX2RhdGUiOnsiZW5hYmxlZCI6ZmFsc2V9LCJtdWx0aXBsZV9saW5rcyI6dHJ1ZSwiZXhwaXJlX2RhdGVfaW50ZXJuYWwiOnsiZW5hYmxlZCI6ZmFsc2V9LCJzZW5kX21haWwiOmZhbHNlLCJ1cGxvYWQiOnRydWUsInVwbG9hZF9maWxlc19kcm9wIjp0cnVlfSwicmVzaGFyaW5nIjp0cnVlLCJ1c2VyIjp7InNlbmRfbWFpbCI6ZmFsc2UsImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwiZ3JvdXBfc2hhcmluZyI6dHJ1ZSwiZ3JvdXAiOnsiZW5hYmxlZCI6dHJ1ZSwiZXhwaXJlX2RhdGUiOnsiZW5hYmxlZCI6dHJ1ZX19LCJkZWZhdWx0X3Blcm1pc3Npb25zIjozMSwiZmVkZXJhdGlvbiI6eyJvdXRnb2luZyI6dHJ1ZSwiaW5jb21pbmciOnRydWUsImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwic2hhcmVlIjp7InF1ZXJ5X2xvb2t1cF9kZWZhdWx0IjpmYWxzZSwiYWx3YXlzX3Nob3dfdW5pcXVlIjp0cnVlfX0sInRoZW1pbmciOnsibmFtZSI6Ik5leHRjbG91ZCIsInVybCI6Imh0dHBzOlwvXC9uZXh0Y2xvdWQuY29tIiwic2xvZ2FuIjoiYSBzYWZlIGhvbWUgZm9yIGFsbCB5b3VyIGRhdGEiLCJjb2xvciI6IiMwMDgyYzkiLCJjb2xvci10ZXh0IjoiI2ZmZmZmZiIsImNvbG9yLWVsZW1lbnQiOiIjMDA4MmM5IiwiY29sb3ItZWxlbWVudC1icmlnaHQiOiIjMDA4MmM5IiwiY29sb3ItZWxlbWVudC1kYXJrIjoiIzAwODJjOSIsImxvZ28iOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL2NvcmVcL2ltZ1wvbG9nb1wvbG9nby5zdmc/dj0wIiwiYmFja2dyb3VuZCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvY29yZVwvaW1nXC9iYWNrZ3JvdW5kLnBuZz92PTAiLCJiYWNrZ3JvdW5kLXBsYWluIjpmYWxzZSwiYmFja2dyb3VuZC1kZWZhdWx0Ijp0cnVlLCJsb2dvaGVhZGVyIjoiaHR0cDpcL1wvd2ViLnRwMi5jZXNpXC9jb3JlXC9pbWdcL2xvZ29cL2xvZ28uc3ZnP3Y9MCIsImZhdmljb24iOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL2NvcmVcL2ltZ1wvbG9nb1wvbG9nby5zdmc/dj0wIn0sInVzZXJfc3RhdHVzIjp7ImVuYWJsZWQiOnRydWUsInN1cHBvcnRzX2Vtb2ppIjp0cnVlfSwid2VhdGhlcl9zdGF0dXMiOnsiZW5hYmxlZCI6dHJ1ZX19">
                                        <input type="hidden" id="initial-state-accessibility-data" value="eyJ0aGVtZSI6ZmFsc2UsImhpZ2hjb250cmFzdCI6ZmFsc2V9">
                                <div class="wrapper">
                        <div class="v-align">
                                                                        <header role="banner">
                                                <div id="header">
                                                        <div class="logo">
                                                                <h1 class="hidden-visually">
                                                                        Nextcloud
</h1>
                                                                                                                        </div>
                                                </div>
                                        </header>
                                                                <main>

<div class="error">
        <h2>Access through untrusted domain</h2>

        <p>
                Please contact your administrator. If you are an administrator, edit the &quot;trusted_domains&quot; setting in config/config.php like the example in config.sample.php.      </p>
        <br />
        <p>
                Further information how to configure this can be found in the <a href="https://docs.nextcloud.com/server/21/go.php?to=admin-trusted-domains" target="blank">documentation</a>.        </p>
</div>
                                </main>
                        </div>
                </div>
                <footer role="contentinfo">
                        <p class="info">
                                <a href="https://nextcloud.com" target="_blank" rel="noreferrer noopener">Nextcloud</a> – a safe home for all your data                       </p>
                </footer>
        </body>
</html>
```
