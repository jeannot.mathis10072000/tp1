###Setup de Docker
```bash
[mathis@docker ~]$ [mathis@docker ~]$ sudo systemctl start docker
[sudo] password for mathis:
[mathis@docker ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
```
```
[mathis@docker ~]$ sudo docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.6.3-docker)
  scan: Docker Scan (Docker Inc., v0.9.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.11
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: runc io.containerd.runc.v2 io.containerd.runtime.v1.linux
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 7b11cfaabd73bb80907dd23182b9347b4245eb5d
 runc version: v1.0.2-0-g52b36a2
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
 Kernel Version: 4.18.0-348.2.1.el8_5.x86_64
 Operating System: Rocky Linux 8.5 (Green Obsidian)
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 809.2MiB
 Name: docker.tp3.cesi
 ID: UHEM:IQFO:KFJE:DC54:LN77:OV3M:YQTT:DHU7:T6KG:4VNO:7SGO:U7CH
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```
###NGINX
```bash
[mathis@docker ~]$ sudo docker pull nginx
[sudo] password for mathis:
Using default tag: latest
latest: Pulling from library/nginx
e5ae68f74026: Pull complete
21e0df283cd6: Pull complete
ed835de16acd: Pull complete
881ff011f1c9: Pull complete
77700c52c969: Pull complete
44be98c0fab6: Pull complete
Digest: sha256:9522864dd661dcadfd9958f9e0de192a1fdda2c162a35668ab6ac42b465f0603
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
[mathis@docker ~]$ sudo usermod -aG docker mathis

[mathis@docker ~]$ sudo docker run --name mathis -d -p 8080:80 nginx
7b5fb62d29d4ca8f358cb9b3430eca7d48d49b2fa19cd1ff62aae5eee8c25dae

[mathis@docker ~]$ sudo docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
7b5fb62d29d4   nginx     "/docker-entrypoint.…"   4 minutes ago   Up 4 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   mathis
```
``` bash
[mathis@docker ~]$ sudo docker run -p 8080:80 -v /home/mathis/docker.conf:/etc/nginx/nginx.conf -d -v /var/www/html/cesi/index.html:/var/www/html/cesi/index.html nginx
3db763c1c6c041599eeeab4df24d7c51bb8761713f92b820f7e2fcffc6d532fa
[mathis@docker ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
3db763c1c6c0   nginx     "/docker-entrypoint.…"   4 seconds ago   Up 3 seconds   0.0.0.0:8080->80/tcp, :::8080->80/tcp   unruffled_euclid
```
###Acces conteneur par bash
```bash
[mathis@docker ~]$ sudo docker exec -it 3db bash
root@3db763c1c6c0:/#
```
###Adresse IP docker
```
"IPAddress": "172.17.0.2"
```
###Limiter l'utilisation de la ram
``` bash
[mathis@docker ~]$ docker run -d --memory=128M nginx
CONTAINER ID   NAME              CPU %     MEM USAGE / LIMIT   MEM %     NET I/O     BLOCK I/O         PIDS
de3f3f316c8e   confident_rubin   0.00%     1.918MiB / 128MiB   1.50%     836B / 0B   8.19kB / 8.19kB   2
```
###Création de MySQL
``` bash 
[mathis@docker ~]$ docker run -it --network wiki --name MySQL -e MYSQL_ROOT_PASSWORD=M0re_Secure -e MYSQL_USER=mathis -e MYSQL_PASSWORD=toto -e MYSQL_DATABASE=wiki -d -p 9249:3306 mysql
a9081be8ba80045932e52128b7d3b60f4c1ea70c3c91b13b46c586ddca52c4a2
[mathis@docker ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                                  NAMES
a9081be8ba80   mysql     "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   33060/tcp, 0.0.0.0:9249->3306/tcp, :::9249->3306/tcp   MySQL
``` 
###Ouverture MySQL
``` bash 
# mysql -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```





















































