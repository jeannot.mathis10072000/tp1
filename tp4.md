###Lancement Vagrant
``` bash 
PS C:\Users\jeann\vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'centos/7' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'centos/7'
    default: URL: https://vagrantcloud.com/centos/7
==> default: Adding box 'centos/7' (v2004.01) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/centos/boxes/7/versions/2004.01/providers/virtualbox.box
Download redirected to host: cloud.centos.org
    default:
    default: Calculating and comparing box checksum...
==> default: Successfully added box 'centos/7' (v2004.01) for 'virtualbox'!
==> default: Importing base box 'centos/7'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: vagrant_default_1639128211458_39838
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
PS C:\Users\jeann\vagrant>
PS C:\Users\jeann\vagrant> vagrant status
Current machine states:

default                   running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to
shut it down forcefully, or you can run `vagrant suspend` to simply
suspend the virtual machine. In either case, to restart it again,
simply run `vagrant up`.
```
###SSh
```bash 
PS C:\Users\jeann\vagrant> vagrant ssh
[vagrant@localhost ~]$
```
```bash 
PS C:\Users\jeann\vagrant> vagrant package --output centos-cesi.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/jeann/vagrant/centos-cesi.box
PS C:\Users\jeann\vagrant> vagrant box add centos-cesi centos-cesi.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos-cesi' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/jeann/vagrant/centos-cesi.box
    box:
==> box: Successfully added box 'centos-cesi' (v0) for 'virtualbox'!
PS C:\Users\jeann\vagrant> vagrant box list
centos-cesi (virtualbox, 0)
centos/7    (virtualbox, 2004.01)
```
###Lancement des 3 VMs
``` bash
PS C:\Users\jeann\vagrant> vagrant status
Current machine states:

node1                     running (virtualbox)
node2                     running (virtualbox)
node3                     running (virtualbox)
```
###TEST PING
```bash
PS C:\Users\jeann\vagrant> vagrant ssh node1
[vagrant@localhost ~]$ ping 10.2.1.102
PING 10.2.1.102 (10.2.1.102) 56(84) bytes of data.
64 bytes from 10.2.1.102: icmp_seq=1 ttl=64 time=0.777 ms
64 bytes from 10.2.1.102: icmp_seq=2 ttl=64 time=0.969 ms
^C
--- 10.2.1.102 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.777/0.873/0.969/0.096 ms
[vagrant@localhost ~]$ ping 10.2.1.103
PING 10.2.1.103 (10.2.1.103) 56(84) bytes of data.
64 bytes from 10.2.1.103: icmp_seq=1 ttl=64 time=0.529 ms
64 bytes from 10.2.1.103: icmp_seq=2 ttl=64 time=0.845 ms
^C
--- 10.2.1.103 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.529/0.687/0.845/0.158 ms
```

```bash

```


























